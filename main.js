const Api_link = 'https://givereceivesyria.onrender.com'
const Domain = 'https://syr-shelters.onrender.com'
let shelters = []
let cities = []
window.addEventListener('DOMContentLoaded', (e) => {
    Init()
})

const Init = async() =>{
    await axios.get(Api_link+'/shelter/').then((res)=>{
        shelters = res.data
        cities = [...new Set(shelters.map(shelter => shelter.city))]
    })
    await axios.get(Domain+'/components/main_page.html').then((page)=>{
        document.querySelector('body').innerHTML = page.data
    }).then(()=>{
        LoadHeader()
    }).then(()=>{
        LoadMain()
    })
}

const LoadHeader = async() =>{
    await axios.get(Domain+'/components/header.html').then((header)=>{
        document.querySelector('#header_container').innerHTML = header.data
    })
}

const LoadMain = async () =>{
    await axios.get(Domain+'/components/main.html').then((main)=>{
        document.querySelector('#main_container').innerHTML = main.data
    }).then(()=>{
        cities.forEach(city => {
            if(city !== undefined) document.querySelector('#cities_select').innerHTML += `<option value="${city}">${city}</option>`
        })
        document.querySelector('#cities_select').addEventListener('change',()=>{
            let selected_city = document.querySelector('#cities_select').value
            let region = document.querySelector('#region_name').value
            LoadShelters(selected_city,region)
        })
        document.querySelector('#region_name').addEventListener('keyup',()=>{
            let selected_city = document.querySelector('#cities_select').value
            let region = document.querySelector('#region_name').value
            LoadShelters(selected_city,region)
        })
    }).then(()=>{
        LoadShelters('all','')
    })
}

const LoadShelters = (city,region) =>{
    document.querySelector('#shelters_container').innerHTML = ''
    let selected_shelters = []
    if(city == 'all' && region == '') selected_shelters = shelters
    else if(city == 'all' && region !== ''){
        selected_shelters = shelters.filter(shelter => shelter.region && shelter.region.includes(region))
    }
    else if (city !== 'all' && region == ''){
        selected_shelters = shelters.filter(shelter => shelter.city && shelter.city === city)
    }
    else{
        selected_shelters = shelters.filter(shelter => shelter.city && shelter.region && shelter.city === city && shelter.region.includes(region))
    }
    document.querySelector('#shelters_count').innerHTML = selected_shelters.length
    selected_shelters.forEach(shlter => {
       
        document.querySelector('#shelters_container').innerHTML += `<tr>
            <td>${shlter.name === undefined ? '-': shlter.name}</td>
            <td>${shlter.contact === undefined ? '-': shlter.contact}</td>
            <td>${shlter.city === undefined ? '-': shlter.city}</td>
            <td>${shlter.region === undefined ? '-': shlter.region}</td>
            <td>${shlter.details === undefined ? '-': shlter.details}</td>
        </tr>`
    })
}